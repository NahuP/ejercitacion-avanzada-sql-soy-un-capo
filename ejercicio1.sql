create table provincias(
    PRO_ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    PRO_DESCRIPCION VARCHAR(45) NOT NULL,
    PRIMARY KEY(PRO_ID)
); 
 

create table partidos(
    PAR_ID int unsigned not null auto_increment,
    PRO_ID int unsigned not null,
    PAR_DESCRIPCION varchar(45) not null, 
    primary key (PAR_ID),
    CONSTRAINT FK_partidos_provincia FOREIGN KEY FK_partidos_provincia (PRO_ID)
        references provincias (PRO_ID)
        on delete restrict
        on update restrict
); 

insert into provincias(PRO_DESCRIPCION) values ('Buenos aires');
insert into provincias(PRO_DESCRIPCION) values ('Santa Cruz');
insert into provincias(PRO_DESCRIPCION) values ('Misiones');
insert into provincias(PRO_DESCRIPCION) values ('Córdoba');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'Morón');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, '3 de febrero');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'La Costa');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'Moreno');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Corpen Aike');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Magallanes');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Río Chico');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Lago Argentino');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Candelarias');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Veinticinco de Mayo');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Iguazú');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Concepción');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'General Roca');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Minas');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Pocho');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Rio Cuarto');


describe partidos;
DESCRIBE provincias; 

select * from provincias 
select * from partidos

delete from provincias 
where PRO_ID=1

delete from provincias 
where PRO_ID=5

delete from partidos
where PRO_ID=4
delete from provincias
where PRO_ID=4

update provincias set PRO_DESCRIPCION='bsas' where PRO_ID=1

update provincias set PRO_DESCRIPCION='buenos mates' where PRO_ID=3

update partidos set PAR_DESCRIPCION='el mejor' where PAR_ID=1

update provincias set PRO_DESCRIPCION='cba' where PRO_ID=2

